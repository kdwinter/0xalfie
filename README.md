# 0xAlfie the Discord bot

This bot was made for personal use, and is currently only really used
on one Discord server. That said, it is built to fully support multiple
servers where possible.

It is built with Ruby via [discordrb](https://github.com/meew0/discordrb).

## Features

* The expected google/google image/youtube search
* Coinflip, rolls, simple arithmetic, etc
* Play any youtube video's audio in your voice channel, in essence a "music bot"
* Play a handful of random pre-defined audio clips, or any given mp3/ogg url
* Pickup group system
* Tracking of messages/message count/bot calls/command usage
* Set individual user permissions, or broader role permissions (for the bot itself)
* Random shenanigans. Just have a look inside the `lib/0xAlfie/containers` directory.

## How to run your own instance:

* Make sure Ruby, Bundler, and SQLite3 are installed on your system
* If you want to use voice functionality, make sure [these things](https://github.com/meew0/discordrb#voice-dependencies) are installed on your system
* Run `git submodule update --recursive --init`
* Run `bundle`
* Make sure `bot.conf` contains the necessary config values. Copy `bot.conf.example` to get started
* Run `bundle exec ruby bin/0xAlfie --setup`
* Add the bot to one of your servers
* Finally, to run the bot itself, run `bundle exec ruby bin/0xAlfie` without any arguments.

## License

See LICENSE.
