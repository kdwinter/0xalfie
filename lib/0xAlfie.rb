require "open-uri"
require "singleton"
require "bundler"
Bundler.require

require_relative "0xAlfie/permission"

module ZxAlfie
  extend self

  NAME = "0xAlfie".freeze
  ROOT = File.dirname(File.expand_path(File.join(__FILE__, ".."))).freeze

  # The bot of bots.
  def bot
    Bot.instance
  end

  # The bot's root path.
  def root
    ROOT
  end

  # Holds configuration information.
  def config
    Configuration.values
  end

  # The Discord user ID of this bot's owner.
  def owner_id
    config[:owner][:id].to_i
  end

  # Prefix used to trigger commands.
  def prefix
    config[:commands][:prefix]
  end

  # Available permission levels.
  PERMISSIONS = [
    Permission.new(1, :pleb,          "Pleb".freeze),
    Permission.new(2, :trusted_user,  "Trusted User".freeze),
    Permission.new(3, :moderator,     "Moderator".freeze),
    Permission.new(4, :administrator, "Administrator".freeze)
  ].freeze

  # Grab permission object by identifier.
  def permission_from_identifier(identifier)
    PERMISSIONS.detect { |p| p.identifier == identifier.to_sym }
  end

  # Grab permission object by level.
  def permission_from_level(level)
    PERMISSIONS.detect { |p| p.level == level.to_i }
  end

  # Shortcut
  def load_container(container)
    Containers.load(container)
  end
end

require_relative "0xAlfie/version"
require_relative "0xAlfie/bot"
require_relative "0xAlfie/configuration"
require_relative "0xAlfie/api"
require_relative "0xAlfie/containers"
