module ZxAlfie
  module API extend self
    def google
      @@google ||= Google::APIClient.new(
        key:                 ZxAlfie.config[:api][:google_api_key],
        authorization:       nil,
        application_name:    ZxAlfie::NAME,
        application_version: ZxAlfie::VERSION
      )
    end

    def youtube
      @@youtube ||= google.discovered_api("youtube", "v3")
    end

    def customsearch
      @@customsearch ||= google.discovered_api("customsearch", "v1")
    end
  end
end
