module ZxAlfie
  class Bot
    # There's always ever 1 bot.
    include Singleton

    # The DiscordRB bot object.
    attr_reader :discord

    # SQLite database object.
    attr_reader :db

    # Additional text-based log file.
    attr_reader :log_file

    # Volume for voice related commands.
    attr_accessor :voice_volume

    # Commands that cannot be overridden by the custom commands container.
    attr_reader :core_commands

    def initialize
      setup_defaults
      setup_database
      setup_discord_bot
      setup_log_file
    end

    # Globally disable voice channel related commands to maintain some peace
    # and quiet.
    def disable_voice!
      @voice_enabled = false
    end

    # Globally re-enable voice channel related commands.
    def enable_voice!
      @voice_enabled = true
    end

    # Is the bot currently allowed to access voice channel related commands?
    def voice_enabled?
      @voice_enabled
    end

    def set_core_commands(commands)
      @core_commands = commands
    end

    # Gracefully shut down.
    def die!
      @closed_by_user = true

      @discord.stop

      periodic_threads.each_value(&:kill)

      @log_file.close if @log_file
      @db.close if @db

      exit 0
    end

    # Shortcut.
    def logger
      Discordrb::LOGGER
    end

    # Did the bot get killed intentionally by user?
    def closed_by_user?
      @closed_by_user
    end

    # Subthreads that run containers' periodic intervals.
    def periodic_threads
      @periodic_threads ||= {}
    end

    def start_periodic_threads
      @periodic_mutex = Mutex.new

      ZxAlfie::Containers.all.select { |container|
        container.respond_to?(:periodic_tick)
      }.each do |container|
        wait =
          if container.const_defined?(:PERIODIC_INTERVAL)
            container::PERIODIC_INTERVAL
          else
            60
          end

        container_name = container.to_s.split("::").last
        periodic_threads[container_name] = Thread.new do
          Thread.current[:discordrb_name] = "cnt_#{container_name}"

          loop do
            @periodic_mutex.synchronize do
              container.periodic_tick
            end

            #logger.info("Ran periodic tick (next in #{wait})")
            sleep(wait)
          end
        end
      end

      periodic_threads.each_value(&:join)
    end

    def bootstrap_containers
      ZxAlfie::Containers.all.each do |container|
        container.bootstrap if container.respond_to?(:bootstrap)
      end
    end

    def cleanup_containers
      ZxAlfie::Containers.all.each do |container|
        container.cleanup if container.respond_to?(:cleanup)
      end
    end

    private def setup_discord_bot
      @discord = Discordrb::Commands::CommandBot.new(
        token:          ZxAlfie.config[:bot][:token],
        client_id:      ZxAlfie.config[:bot][:client_id],
        prefix:         ZxAlfie.prefix,
        log_mode:       :verbose,
        fancy_log:      true,
        #help_command:   false,
        advanced_functionality: false
      )

      class << @discord
        def logger
          Discordrb::LOGGER
        end
      end
    end

    private def setup_log_file
      @log_file = File.open(File.join(ZxAlfie.root, "bot.log"), "w+")

      # Add the file log
      if File.readable?(@log_file) && File.writable?(@log_file)
        logger.streams << @log_file
        logger.good("Bot started by UID #{Process.uid} at #{Time.now.to_i} " \
                    "on Ruby #{RUBY_VERSION}")
      else
        logger.warn("Couldn't add file based log stream -- not writable or " \
                    "readable.")
      end
    end

    private def setup_database
      @db = SQLite3::Database.new(File.join(ZxAlfie.root, "alfie.sqlite"))
      if @db.readonly?
        logger.error("Can't write to database.")
        exit 1
      end

      @db.results_as_hash  = true
      @db.type_translation = true

      @db.execute_batch(File.read(File.join(ZxAlfie.root, "schema.sql")))
    end

    private def setup_defaults
      @voice_volume   = 0.10
      @voice_enabled  = true
      @core_commands  = []
      @closed_by_user = false
    end
  end
end
