module ZxAlfie
  module Configuration
    class << self
      @@configuration = nil

      def load_from_file
        @@configuration = ParseConfig.new(File.join(ZxAlfie.root, "bot.conf"))
      rescue
        ZxAlfie.bot.logger.error("Please make sure `bot.conf` exists and is " \
                                 "readable.")
        exit 1
      end

      def values
        if @@configuration.nil?
          load_from_file
          @@configuration = deep_symbolize_keys(@@configuration.params)
        end

        @@configuration
      end

      # The bot won't run without some of its config variables being set,
      # this method will recursively check if given keys are present
      def assert_value_presence(*keys)
        cfg = values.dup

        keys.each do |key|
          val = cfg[key.to_sym]

          if val.nil? || val == "".freeze
            ZxAlfie.bot.logger.error("`#{keys.join(".")}' config value not " \
                                     "set. This bot shall not run.")
            exit 1
          end

          # Go deeper down
          cfg = val
        end

        true
      end

      private def deep_symbolize_keys(hash)
        result = {}

        hash.each do |key, val|
          val = deep_symbolize_keys(val) if val.is_a?(Hash)
          result[key.to_sym] = val
        end

        result
      end
    end
  end
end
