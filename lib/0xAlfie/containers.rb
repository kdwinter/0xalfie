module ZxAlfie
  module Containers
    # Array of all submodules (they should all be containers).
    def self.all
      constants.map { |c| const_get(c) }
    end

    # Load a command container.
    def self.load(container)
      begin
        require_relative "./containers/#{container.to_s.underscore}.rb"
        ZxAlfie.bot.discord.include!(const_get(container))
      rescue LoadError => e
        ZxAlfie.bot.logger.warn("Failed to load container #{container} " \
                                "(#{e.message}).")
      end
    end
  end
end
