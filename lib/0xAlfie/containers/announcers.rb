module ZxAlfie
  module Containers
    module Announcers
      extend Discordrb::EventContainer

      member_join do |event|
        unless event.user.current_bot?
          if ZxAlfie::Containers.const_defined?(:Users)
            ZxAlfie::Containers::Users.kicked_users.delete_if { |u|
              u.id == event.user.id
            }
          end

          channel = event.server.general_channel
          if ZxAlfie.bot.discord.profile.on(event.server).permission?(
            :send_messages, channel
          )
            channel.send_message "**#{event.user.name}** joined the server. " \
              "Welcome!"
          end
        end
      end

      member_leave do |event|
        unless event.user.current_bot?
          channel = event.server.general_channel

          if ZxAlfie.bot.discord.profile.on(event.server).permission?(
            :send_messages, channel
          )
            channel.send_message "**#{event.user.name}** left the server. Bye!"
          end
        end
      end
    end
  end
end
