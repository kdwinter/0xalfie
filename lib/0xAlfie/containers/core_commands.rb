module ZxAlfie
  module Containers
    module CoreCommands
      extend Discordrb::Commands::CommandContainer

      command(:disablevoice,
        description:          "Disable all voice commands.",
        usage:                "#{ZxAlfie.prefix}disablevoice",
        help_available:       false,
        min_args:             0,
        max_args:             0,
        permission_level:     ZxAlfie.permission_from_identifier(
          :administrator
        ).level,
        permission_message:   false
      ) do |event|
        ZxAlfie.bot.disable_voice!

        if ZxAlfie.bot.discord.profile.on(event.server).permission?(
          :send_messages, event.channel
        )
          next "All voice channel related commands are now disabled."
        end

        nil
      end

      command(:enablevoice,
        description:          "Enable all voice commands.",
        usage:                "#{ZxAlfie.prefix}enablevoice",
        help_available:       false,
        min_args:             0,
        max_args:             0,
        permission_level:     ZxAlfie.permission_from_identifier(
          :administrator
        ).level,
        permission_message:   false
      ) do |event|
        ZxAlfie.bot.enable_voice!

        if ZxAlfie.bot.discord.profile.on(event.server).permission?(
          :send_messages, event.channel
        )
          next "All voice channel related commands have been re-enabled."
        end

        nil
      end

      command(:prune,
        description:          "Prune set amount of messages.",
        usage:                "#{ZxAlfie.prefix}prune 5",
        help_available:       true,
        min_args:             1,
        max_args:             1,
        required_permissions: [:manage_messages],
        permission_level:     ZxAlfie.permission_from_identifier(
          :administrator
        ).level,
        permission_message:   false
      ) do |event, amount|
        amount = amount.to_i + 1
        amount = 2 if amount < 2

        while amount > 100
          event.channel.prune(100)
          amount -= 100
        end

        event.channel.prune(amount)
        nil
      end

      command(:topic,
        description:          "Set the channel's topic",
        min_args:             1,
        permission_level:     ZxAlfie.permission_from_identifier(
          :moderator
        ).level,
        permission_message:   "Can't let you do that.",
        required_permissions: [:manage_channels],
        help_available:       true
      ) do |event, *text|
        text = text.join(" ")
        event.channel.topic = text
        if ZxAlfie.bot.discord.profile.on(event.server).permission?(
          :send_messages, event.channel
        )
          "Topic changed to **#{text}**"
        end
      end

      command(:avatar,
        description:        "Set the bot's avatar.",
        min_args:           1,
        max_args:           1,
        permission_level:   ZxAlfie.permission_from_identifier(
          :administrator
        ).level,
        permission_message: "I'm beautiful the way I am.",
        help_available:     false
      ) do |event, url|
        begin
          ZxAlfie.bot.discord.profile.avatar = open(url)
        rescue StandardError => e
          if ZxAlfie.bot.discord.profile.on(event.server).permission?(
            :send_messages, event.channel
          )
            "Something went wrong setting the avatar: `#{e.message}`"
          end
        end
        if ZxAlfie.bot.discord.profile.on(event.server).permission?(
          :send_messages, event.channel
        )
          "New avatar should be set."
        end
      end

      command(:die,
        description:        "Kill the bot.",
        help_available:     false,
        permission_level:   ZxAlfie.permission_from_identifier(
          :administrator
        ).level,
        permission_message: "ok :gun:"
      ) do |event|
        next unless event.user.id == ZxAlfie.owner_id

        if ZxAlfie.bot.discord.profile.on(event.server).permission?(
          :manage_messages, event.channel
        )
          event.message.delete
        end

        if ZxAlfie.bot.discord.profile.on(event.server).permission?(
          :send_messages, event.channel
        )
          ZxAlfie.bot.discord.send_message(event.channel.id, ":gun:")
        end

        ZxAlfie.bot.die!
      end
    end
  end
end
