module ZxAlfie
  module Containers
    module Loggers
      extend Discordrb::EventContainer

      COMMAND_EXTRACT_RE = /\!([a-z0-9\-_]+)/i
      message(private: false) do |event|
        content = event.message.content
        details = [event.channel.id, event.user.id, event.server.id,
                   Time.now.strftime("%Y-%m-%d %H:%M:%S")]

        Discordrb::LOGGER.info("#{event.server.name} #{event.channel.name} " \
                               "#{event.user.name}: #{content}")

        if content.start_with?(ZxAlfie.prefix)
          command = content.match(COMMAND_EXTRACT_RE)[1]
          ZxAlfie.bot.db.execute(
            "INSERT INTO bot_calls (command, channel_id, user_id, server_id, " \
            "called_at) VALUES (?, ?, ?, ?, ?)",
            [command] + details
          )
        else
          ZxAlfie.bot.db.execute(
            "INSERT INTO messages (content, channel_id, user_id, server_id, " \
            "sent_at) VALUES (?, ?, ?, ?, ?)",
            [content] + details
          )
        end
      end

      presence do |event|
        ZxAlfie.bot.db.execute("DELETE FROM seen WHERE user_id = ? AND " \
                               "server_id = ?", event.user.id, event.server.id)
        ZxAlfie.bot.db.execute(
          "INSERT INTO seen (user_id, server_id, status, seen_at) VALUES " \
          "(?, ?, ?, ?)",
          [event.user.id, event.server.id, event.status.to_s,
           Time.now.strftime("%Y-%m-%d %H:%M:%S")]
        )
      end
    end
  end
end
