module ZxAlfie
  module Containers
    module Memes
      extend Discordrb::EventContainer

      @@rate_limiter = Discordrb::Commands::SimpleRateLimiter.new
      @@rate_limiter.bucket(:memes, delay: 3)

      message(
        starting_with: /sure\z/i,
      ) do |event|
        next if @@rate_limiter.rate_limited?(:memes, event.channel)

        if ZxAlfie.bot.discord.profile.on(event.server).permission?(:embed_links,
                                                                    event.channel)
          event.channel.send_message(
            "https://media.giphy.com/media/Fml0fgAxVx1eM/giphy.gif".freeze
          )
        end

        nil
      end

      message(
        with_text: "FeelsBadMan",
      ) do |event|
        next if @@rate_limiter.rate_limited?(:memes, event.channel)
        if ZxAlfie.bot.discord.profile.on(event.server).permission?(:embed_links,
                                                                    event.channel)
          event.channel.send_message(
            "https://openclipart.org/image/2400px/svg_to_png/222252/feels.png".freeze
          )
        end

        nil
      end

      #INSULTS = [
      #  "Really?".freeze,
      #  "/point; /laugh".freeze,
      #  "Have they been introduced to Dota2?".freeze
      #].freeze
      #playing(
      #  game: "League of Legends"
      #) do |event|
      #  event.server.general_channel.send_message "**#{event.user.name}** just launched LoL. #{INSULTS.sample}."
      #end
    end
  end
end
