module ZxAlfie
  module Containers
    module Music
      extend Discordrb::Commands::CommandContainer

      @@rate_limiter = Discordrb::Commands::SimpleRateLimiter.new
      @@rate_limiter.bucket(:music, delay: 5)

      URL_RE = URI.regexp(['http', 'https'])
      DOWNLOAD_DIR = File.join(ZxAlfie.root, "youtube-dl")

      @@playing = false

      command(:playsong,
        description:          "Play the sound of a YouTube video or mp3 url.",
        usage:                "#{ZxAlfie.prefix}playsong big butts",
        help_available:       true,
        min_args:             1,
        required_permissions: [:speak],
        permission_level:     ZxAlfie.permission_from_identifier(:trusted_user).level,
        permission_message:   false,
      ) do |event, *song|
        next if @@rate_limiter.rate_limited?(:music, event.user)
        next unless ZxAlfie.bot.voice_enabled?

        if @@playing
          next "Still downloading or playing another song. Ignoring this."
        end

        can_send_message = ZxAlfie.bot.discord.profile.on(event.server).
          permission?(:send_messages, event.channel)

        begin
          @@playing = true

          song = song.join(" ")
          if song =~ /(ten|10)\shour/i
            next "Please have mercy :( not downloading that..."
          end

          # Another voice event could be currently active anyhow
          if event.voice
            event.voice.stop_playing
            event.voice.destroy
          end

          if channel = event.user.voice_channel
            begin
              # Play a .mp3 or .ogg url directly as an IO stream.
              if song =~ URL_RE && (song.end_with?(".mp3") || song.end_with?(".ogg"))
                voice_bot = ZxAlfie.bot.discord.voice(channel) ||
                  ZxAlfie.bot.discord.voice_connect(channel)
                voice_bot.volume = ZxAlfie.bot.voice_volume

                voice_bot.play_io(open(song))
                voice_bot.stop_playing
                voice_bot.destroy
              else
                result = ZxAlfie::API.google.execute!(
                  api_method: ZxAlfie::API.youtube.search.list, parameters: {
                    part: "snippet", q: song, maxResults: 1, type: "video",
                    safeSearch: "none"
                  }
                )

                if result = result.data.items.first
                  id = result.id.videoId
                  if ["JoQ4GidQP-k", "XrIkPjHLlGU"].include?(id)
                    next "KILL YOURSELF"
                  end

                  title = result.snippet.title
                  if title =~ /(easy street|walking dead)/i
                    ZxAlfie.bot.discord.ignore_user(event.user)
                    ZxAlfie::Containers::Users.silenced_users[event.user.id] = Time.now.to_i + 300

                    next "STOP IT that's a 5 minute silence for you **#{event.user.name}**"
                  end

                  download_path = File.join(DOWNLOAD_DIR, id)

                  if file = get_saved_file(download_path)
                    event.channel.send_message "Playing #{title}..." if can_send_message
                  else
                    url = "https://youtube.com/watch?v=#{id}"
                    event.channel.send_message "Downloading #{url}" if can_send_message

                    dl_opts = {
                      output: "#{download_path}.ogg", format: :bestaudio,
                      extract_audio: true, audio_format: "vorbis"
                    }
                    dl_opts[:"max-filesize"] = "10m" unless event.user.owner?

                    song = YoutubeDL.download(url, dl_opts)
                    filesize = song.information[:filesize]
                    ZxAlfie.bot.logger.info "Downloaded #{song.title} " \
                      "(duration: #{song.duration} - id: #{song.information[:id]}" \
                      " - size: #{convert_bytes_to_megabytes(filesize)}"

                    # Filesize/duration checks, except for bot owner.
                    unless event.user.owner?
                      if filesize && (filesize >= max_file_size)
                        delete_existing_file(download_path)
                        next "This song's filesize is too big (max " \
                          "#{convert_bytes_to_megabytes(max_file_size)}. Given" \
                          ": #{convert_bytes_to_megabytes(filesize)})."
                      end

                      if song.duration > max_duration
                        delete_existing_file(download_path)
                        next "This song is too long (max #{max_duration} seconds" \
                          ". Given: #{song.duration} seconds)."
                      end
                    end

                    event.channel.send_message "Done! Playing #{title}..." if can_send_message
                  end

                  voice_bot = ZxAlfie.bot.discord.voice(channel) ||
                    ZxAlfie.bot.discord.voice_connect(channel)
                  voice_bot.volume = ZxAlfie.bot.voice_volume

                  if file = get_saved_file(download_path)
                    voice_bot.play_file(file)
                  end

                  voice_bot.stop_playing
                  voice_bot.destroy
                else
                  next "Couldn't find that song."
                end
              end
            rescue Cocaine::ExitStatusError, StandardError => e
              ZxAlfie.bot.discord.user(ZxAlfie.owner_id).pm("Error while " \
                                                            "playing song:")
              Discordrb.split_message(e.message).each do |chunk|
                ZxAlfie.bot.discord.user(ZxAlfie.owner_id).pm(chunk)
              end

              next "Something went wrong trying to play/download song. " \
                "Error was logged."
            end
          else
            next "You're not in any voice channel..."
          end

          nil
        ensure
          @@playing = false
        end
      end

      command(:volume,
        description:          "Set voice bot's volume.",
        usage:                "#{ZxAlfie.prefix}volume 50%",
        help_available:       true,
        min_args:             1,
        max_args:             1,
        permission_level:     ZxAlfie.permission_from_identifier(:trusted_user).level,
        permission_message:   false
      ) do |event, volume|
        next if @@rate_limiter.rate_limited?(:music, event.user)
        next unless ZxAlfie.bot.voice_enabled?

        volume = volume.to_f
        volume = volume / 100
        volume = 1 if volume > 1

        if event.voice
          event.voice.volume = volume
        end
        ZxAlfie.bot.voice_volume = volume

        nil
      end

      command(:seeksong,
        description:          "Skip ahead to the given amount of second in " \
                              "the currently playing song.",
        usage:                "#{ZxAlfie.prefix}seeksong 50",
        help_available:       true,
        min_args:             1,
        max_args:             1,
        permission_level:     ZxAlfie.permission_from_identifier(:trusted_user).level,
        permission_message:   false
      ) do |event, seconds|
        next if @@rate_limiter.rate_limited?(:music, event.user)
        next unless ZxAlfie.bot.voice_enabled?

        if event.voice
          event.voice.skip(seconds.to_i)
          nil
        else
          if ZxAlfie.bot.discord.profile.on(event.server).permission?(:send_messages,
                                                                      event.channel)
            "No song is currently playing."
          end
        end
      end

      command(:pausesong,
        description:          "Pause the currently playing song.",
        usage:                "#{ZxAlfie.prefix}pausesong",
        help_available:       true,
        min_args:             0,
        max_args:             0,
        permission_level:     ZxAlfie.permission_from_identifier(:trusted_user).level,
        permission_message:   false
      ) do |event|
        next if @@rate_limiter.rate_limited?(:music, event.user)
        next unless ZxAlfie.bot.voice_enabled?

        if event.voice
          event.voice.pause
          nil
        else
          if ZxAlfie.bot.discord.profile.on(event.server).permission?(:send_messages,
                                                                      event.channel)
            "No song is currently playing."
          end
        end
      end

      command(:continuesong,
        description:          "Continue the currently paused song.",
        usage:                "#{ZxAlfie.prefix}continuesong",
        help_available:       true,
        min_args:             0,
        max_args:             0,
        permission_level:     ZxAlfie.permission_from_identifier(:trusted_user).level,
        permission_message:   false
      ) do |event|
        next if @@rate_limiter.rate_limited?(:music, event.user)
        next unless ZxAlfie.bot.voice_enabled?

        if event.voice
          event.voice.continue
          nil
        else
          if ZxAlfie.bot.discord.profile.on(event.server).permission?(:send_messages,
                                                                      event.channel)
            "No song is currently playing."
          end
        end
      end

      command(:stopsong,
        description:          "Stop the currently playing song.",
        usage:                "#{ZxAlfie.prefix}stopsong",
        help_available:       true,
        min_args:             0,
        max_args:             0,
        required_permissions: [:send_messages],
        permission_level:     ZxAlfie.permission_from_identifier(:trusted_user).level,
        permission_message:   false
      ) do |event|
        next if @@rate_limiter.rate_limited?(:music, event.user)
        next unless ZxAlfie.bot.voice_enabled?

        if event.voice
          event.voice.stop_playing
          @@playing = false
          event.voice.destroy
          nil
        else
          if ZxAlfie.bot.discord.profile.on(event.server).permission?(:send_messages,
                                                                      event.channel)
            "No song is currently playing."
          end
        end
      end

      ### Helpers

      def self.convert_bytes_to_megabytes(bytes)
        bytes = bytes.to_i
        if bytes < 1024
          to_s + "B"
        elsif bytes < (1024 * 1024)
          (bytes / 1024).to_s + "KB"
        elsif bytes < (1024 * 1024 * 1024)
          (bytes / 1024 / 1024).to_s + "MB"
        else
          (bytes / 1024 / 1024 / 1024).to_s + "GB"
        end
      end

      def self.max_file_size
        ZxAlfie.config[:"youtube-dl"][:max_file_size].to_i
      end

      def self.max_duration
        ZxAlfie.config[:"youtube-dl"][:max_duration].to_i
      end

      def self.get_saved_file(path)
        path = Dir["#{path}*"].first || "#{path}.ogg"
        File.exist?(path) ? path : nil
      end

      def self.delete_existing_file(path)
        if f = get_saved_file(path)
          File.delete(f)
        end
      end
    end
  end
end
