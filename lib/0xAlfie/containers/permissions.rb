module ZxAlfie
  module Containers
    module Permissions
      extend Discordrb::Commands::CommandContainer

      def self.bootstrap
        # Make sure the owner is automatically admin.
        ZxAlfie.bot.discord.set_user_permission(
          ZxAlfie.owner_id,
          ZxAlfie.permission_from_identifier(:administrator).level
        )

        # NOTE: Set per server (once Discordb supports this)?
        ZxAlfie.bot.db.execute(
          "SELECT user_id, identifier FROM user_permissions"
        ) do |row|
          ZxAlfie.bot.discord.set_user_permission(
            row["user_id"].to_i,
            ZxAlfie.permission_from_identifier(row["identifier"]).level
          )
        end
        ZxAlfie.bot.db.execute(
          "SELECT role_id, identifier FROM role_permissions"
        ) do |row|
          ZxAlfie.bot.discord.set_role_permission(
            row["role_id"].to_i,
            ZxAlfie.permission_from_identifier(row["identifier"]).level
          )
        end
      end

      command(:permissions,
        description:          "List available permissions for this bot.",
        usage:                "#{ZxAlfie.prefix}permissions",
        min_args:             0,
        max_args:             0,
        help_available:       true,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event|
        message = "**List of available permissions:**\n"
        PERMISSIONS.each do |permission|
          message << "#{permission.level}: **#{permission.title}** " \
            "(`#{permission.identifier}`)\n"
        end

        event.channel.send_message(message)
        nil
      end

      command(:mypermission,
        description:          "Check your bot permissions on this server.",
        usage:                "#{ZxAlfie.prefix}mypermission",
        help_available:       true,
        min_args:             0,
        max_args:             0,
      ) do |event|
        permissions = ZxAlfie.bot.discord.instance_variable_get(:@permissions)
        user = event.user

        role_level = user.roles.reduce(0) do |memo, role|
          [permissions[:roles][role.id] || 0, memo].max
        end
        highest_level = [permissions[:users][user.id] || 0, role_level].max

        event.user.pm "Your permission level is **" \
          "#{ZxAlfie.permission_from_level(highest_level).title}** " \
          "(#{highest_level})."
        nil
      end

      command(:setuserpermission,
        description:          "Set permissions for a specific user, by " \
                              "permission identifier and user name.",
        usage:                "#{ZxAlfie.prefix}setuserpermission trusted_user" \
                              "User Name",
        help_available:       false,
        min_args:             2,
        permission_level:     ZxAlfie.permission_from_identifier(
          :administrator
        ).level,
        permission_message:   "Nope."
      ) do |event, permission_identifier, *name|
        can_send_message = ZxAlfie.bot.discord.profile.on(event.server).
          permission?(:send_messages, event.channel)

        name = name.join(" ")
        member = event.server.members.detect do |m|
          m.name.downcase == name.downcase
        end

        if member.nil?
          next "Couldn't find **#{name}** in this server." if can_send_message
        end

        permission = ZxAlfie.permission_from_identifier(permission_identifier)
        if permission.nil?
          next "`#{permission_identifier}` is not a valid permission. Check " \
            "`!permissions` for a list."
        end

        if member && permission
          unless member.id == ZxAlfie.owner_id || member.current_bot?
            ZxAlfie.bot.db.execute(
              "DELETE FROM user_permissions WHERE user_id = ?",
              member.id
            )
            ZxAlfie.bot.db.execute(
              "INSERT INTO user_permissions (user_id, server_id, identifier, " \
              "set_by_user_id) VALUES (?, ?, ?, ?)",
              [member.id, event.server.id, permission.identifier.to_s,
               event.user.id]
            )
            ZxAlfie.bot.discord.set_user_permission(member.id, permission.level)

            if can_send_message
              event.channel.send_message "**#{member.name}** now has " \
                "`#{permission.identifier}` permissions."
            end
          else
            if can_send_message
              event.channel.send_message "**#{member.name}** doesn't need " \
                "any permissions."
            end
          end
        end

        nil
      end

      command(:removeuserpermission,
        description:          "Remove permissions from given user name.",
        usage:                "#{ZxAlfie.prefix}removeuserpermission User Name",
        help_available:       false,
        min_args:             1,
        permission_level:     ZxAlfie.permission_from_identifier(
          :administrator
        ).level,
        permission_message:   "Nope."
      ) do |event, *name|
        can_send_message = ZxAlfie.bot.discord.profile.on(event.server).
          permission?(:send_messages, event.channel)

        name = name.join(" ")
        if member = event.server.members.detect { |m|
          m.name.downcase == name.downcase
        }
          unless member.id == ZxAlfie.owner_id || member.current_bot?
            ZxAlfie.bot.db.execute(
              "DELETE FROM user_permissions WHERE user_id = ?",
              member.id
            )
            ZxAlfie.bot.discord.instance_variable_get(:@permissions)[:users].
              delete(member.id)
            if can_send_message
              event.channel.send_message "Removed permissions " \
                "**#{member.name}** (if any)."
            end
          else
            event.channel.send_message "Won't remove these." if can_send_message
          end
        else
          event.channel.send_message "Couldn't find **#{name}** in this server." if can_send_message
        end

        nil
      end

      command(:setrolepermission,
        description:          "Set permissions for a specific role, by " \
                              "permission identifier and role name.",
        usage:                "#{ZxAlfie.prefix}setrolepermission trusted_user" \
                              "Role Name",
        help_available:       false,
        min_args:             2,
        permission_level:     ZxAlfie.permission_from_identifier(
          :administrator
        ).level,
        permission_message:   "Nope."
      ) do |event, permission_identifier, *name|
        can_send_message = ZxAlfie.bot.discord.profile.on(event.server).
          permission?(:send_messages, event.channel)

        name = name.join(" ")
        role = event.server.roles.detect { |r| r.name.downcase == name.downcase }

        if role.nil?
          next "Couldn't find **#{name}** role in this server."
        end

        permission = ZxAlfie.permission_from_identifier(permission_identifier)
        if permission.nil?
          next "`#{permission_identifier}` is not a valid permission. Check" \
            "`!permissions` for a list."
        end

        if role && permission
          ZxAlfie.bot.db.execute(
            "DELETE FROM role_permissions WHERE role_id = ?",
            role.id
          )
          ZxAlfie.bot.db.execute(
            "INSERT INTO role_permissions (role_id, server_id, identifier, " \
            "set_by_user_id) VALUES (?, ?, ?, ?)",
            [role.id, event.server.id, permission.identifier.to_s,
             event.user.id]
          )
          ZxAlfie.bot.discord.set_role_permission(role.id, permission.level)

          event.channel.send_message "Users with role **#{role.name}** now have " \
            "`#{permission.identifier}` permissions." if can_send_message
        end

        nil
      end

      command(:removerolepermission,
        description:          "Remove permissions from given role name.",
        usage:                "#{ZxAlfie.prefix}removerolepermission Role Name",
        help_available:       false,
        min_args:             1,
        permission_level:     ZxAlfie.permission_from_identifier(
          :administrator
        ).level,
        permission_message:   "Nope."
      ) do |event, *name|
        can_send_message = ZxAlfie.bot.discord.profile.on(event.server).
          permission?(:send_messages, event.channel)

        name = name.join(" ")
        if role = event.server.roles.detect { |r|
          r.name.downcase == name.downcase
        }
          ZxAlfie.bot.db.execute(
            "DELETE FROM role_permissions WHERE role_id = ?",
            role.id
          )
          ZxAlfie.bot.discord.instance_variable_get(:@permissions)[:roles].
            delete(role.id)

          event.channel.send_message "Removed **#{role.name}**'s permissions (if any)." if can_send_message
        else
          event.channel.send_message "Couldn't find **#{name}** in this server." if can_send_message
        end

        nil
      end
    end
  end
end
