module ZxAlfie
  module Containers
    module Private
      extend Discordrb::EventContainer

      # This is a bunch of fuckery
      private_message do |event|
        # When PM-ing the bot as the owner
        if event.user.id == ZxAlfie.owner_id
          case event.message.content
          # Bot uptime
          when /^uptime$/
            uptime = Time.now.to_i - START
            uptime = "%d days, %d hours, %d minutes, %d seconds" % [
              uptime.to_i / 86400, (uptime % 86400).to_i / 3600,
              *((uptime % 3600).divmod(60))
            ]

            event.user.pm "Uptime: #{uptime}"

          # Who has which permissions?
          when /perms/, /permissions/
            permissions = ZxAlfie.bot.discord.
              instance_variable_get(:@permissions)
            roles = ""

            ZxAlfie.bot.discord.servers.values.each do |server|
              perms = permissions[:roles].map { |id, level|
                ("#{server.roles.detect { |r| r.id == id }.name}: " \
                 "#{level}" rescue "#{id} doesn't exist here")
              }.join(", ")
              roles += "**Role permissions on #{server.name}:** #{perms}  \n"
            end

            user_perms = permissions[:users].map { |id, level|
              "#{ZxAlfie.bot.discord.user(id).name}: #{level}"
            }.join(", ")
            event.user.pm "**User permissions:** #{user_perms}  \n" + roles

          # List the servers the bot is in
          when /^servers$/
            servers = ZxAlfie.bot.discord.servers.values.map { |s|
              "**#{s.name}** (`#{s.id}`)"
            }.join(", ")
            event.user.pm "I'm in these servers: #{servers}"

          # Tell the bot to leave a server
          when /^leaveserver/
            # leaveserver 120751204712
            server_id = event.message.content.split(" ").last

            if server = ZxAlfie.bot.discord.servers[server_id.to_i]
              server.leave
              event.user.pm "I have left server **#{server.name}**."
            else
              event.user.pm "I'm not in that server, I think..."
            end

          # FIXME: Why does discord say the bot doesn't have required permissions?
          #when /^join/
          #  # join <invite link>
          #  if invite = ZxAlfie.bot.discord.resolve_invite_code(
          #    event.message.content.split(" ").last
          #  )
          #    ZxAlfie.bot.discord.join(invite)
          #    event.user.pm "I should be in your server now!"
          #  end

          # Grab the invite URL
          when /^invite$/
            event.user.pm "Try clicking this link: " \
              "#{ZxAlfie.bot.discord.invite_url}"
          else
            event.user.pm "Yes, master?"
          end

        # When PM-ing the bot as regular user
        else
          case event.message.content
          # Check your own permissions
          when /perms/i, /permissions/i
            permissions = ZxAlfie.bot.discord.
              instance_variable_get(:@permissions)
            user = event.user
            highest_level = permissions[:users][user.id] || 0
            level = (
              ZxAlfie.permission_from_level(highest_level).title rescue "unknown"
            )

            user.pm "Your individual permission level is **#{level}** (" \
              "#{highest_level})."

          # Grab the invite URL
          when /^invite$/
            event.user.pm "Try clicking this link: " \
              "#{ZxAlfie.bot.discord.invite_url}"
          else
            event.user.pm "I'm BUSY! :rage:"
          end
        end
      end

      mention do |event|
        if ZxAlfie.bot.discord.profile.on(event.server).permission?(
          :send_messages, event.channel
        )
          case event.message.content
          when /fuck\s+(you|u)/i
            "Fuck you too, **#{event.user.name}** :rage:"
          else
            "No."
          end
        end
      end
    end
  end
end
