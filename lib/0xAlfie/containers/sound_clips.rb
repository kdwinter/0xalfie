module ZxAlfie
  module Containers
    module SoundClips
      extend Discordrb::Commands::CommandContainer

      command(:silence,
        description:          "Lay down the law and silence your voice channel.",
        usage:                "#{ZxAlfie.prefix}silence",
        help_available:       true,
        min_args:             0,
        max_args:             0,
        required_permissions: [:speak, :manage_channels, :manage_server],
        permission_level:     ZxAlfie.permission_from_identifier(
          :trusted_user
        ).level,
        permission_message:   false
      ) do |event|
        next unless ZxAlfie.bot.voice_enabled?

        can_send_message = ZxAlfie.bot.discord.profile.on(event.server).
          permission?(:send_messages, event.channel)

        if event.voice
          event.voice.stop_playing
          event.voice.destroy
        end

        if channel = event.user.voice_channel
          if can_send_message
            event.channel.send_message(
              "https://hydra-media.cursecdn.com/dota2.gamepedia.com/4/43/Silencer.png".freeze
            )
          end

          # Need to thread this because it takes 1+ sec per muted person,
          # blocking further execution until it's finished muting everyone.
          # Could become very slow in large voice channels.

          t = []
          users_to_mute = channel.users.reject { |u| u.owner? || u.self_muted? }

          begin
            users_to_mute.each do |user|
              t << Thread.new { user.server_mute }
            end
            t.each(&:join)

            voice_bot = ZxAlfie.bot.discord.voice(channel) ||
              ZxAlfie.bot.discord.voice_connect(channel)#, false)
            voice_bot.play_file(File.join(ZxAlfie.root, "mp3", "silence.mp3"))

          rescue StandardError => e
            if can_send_message
              event << "Something went wrong. Sigh. The error was logged."
            end
            ZxAlfie.bot.discord.user(ZxAlfie.owner_id).pm "Error while " \
              "silencing: `#{e.message[0..1900]}`"
          ensure
            t.clear
            users_to_mute.each do |user|
              t << Thread.new { user.server_unmute }
            end
            t.each(&:join)
          end

          voice_bot.destroy
          #event << "Done :gun:"
        end
        nil
      end

      CLIPS = [{
        command:     :top,
        description: "Gigamo on voip when he needs help.",
        file:        "b-toptoptoptop.mp3",
        image:       "http://www.gosugamers.net/files/images/news/2015/august/alliance%20ti3.jpg"
      }, {
        command:     :moan,
        description: "Get your moan going.",
        file:        "moan.mp3"
      }, {
        command:     :thanks,
        description: "When Gigamo tries to help.",
        file:        "b-help.mp3"
      }, {
        command:     :wow,
        description: "Pro plays.",
        file:        "ld-wow.mp3"
      }, {
        command:     :back,
        description: "Get the fuck back.",
        file:        "e-back.mp3"
      }, {
        command:     :fight,
        description: "FIGHT!",
        file:        "fight.mp3"
      }, {
        command:     :blbl,
        description: "blblblblbl",
        file:        "blbl.mp3"
      }, {
        command:     :defend,
        description: "DEFEND THE BASE",
        file:        "defend.mp3"
      }, {
        command:     :toobad,
        description: "BibleThump",
        file:        "sadviolin.mp3"
      }, {
        command:     :allah,
        description: "For Allah",
        file:        "allah.mp3"
      }, {
        command:     :airhorn,
        description: "Sound of our ppl",
        file:        "airhorn.mp3"
      }, {
        command:     :cans,
        description: "PUT YOUR CANS IN THE AIR",
        file:        "cans_in_the_air.mp3"
      }, {
        command:     :ready,
        description: "Ready?",
        file:        "ready.mp3"
      }, {
        command:     :war,
        description: "real bloody war",
        file:        "real_bloody_war.mp3"
      }, {
        command:     :balls,
        description: "Balls of steel",
        file:        "ive_got_balls_of_steel.mp3",
        image:        "http://i.imgur.com/D7Ux9uW.gifv"
      }, {
        command:     :bra,
        description: "BRA BRA BRA",
        file:        "bra.mp3"
      }, {
        command:     :itstime,
        description: "Its time",
        file:        "its_time.mp3"
      }, {
        command:     :eatshitanddie,
        description: "Eat shit and die",
        file:        "die.mp3"
      }, {
        command:     :nope,
        description: "that aint right",
        file:        "nope.mp3"
      }, {
        command:     :neck,
        description: "ill rip your head off and shit down your neck",
        file:        "neck.mp3"
      }, {
        command:     :smells,
        description: "Something smells rotten...",
        file:        "smells.mp3"
      }, {
        command:     :slap,
        description: "Dick slap some nigga",
        file:        "slap.mp3",
        image:       "https://c1.staticflickr.com/5/4133/4952745966_f3ff3909bc_m.jpg"
      }, {
        command:     :alldead,
        description: "They're all dead!",
        file:        "Chatwheel_all_dead.wav"
      }, {
        command:     :ayayay,
        description: "Cyka",
        file:        "Chatwheel_ay_ay_ay.wav"
      }, {
        command:     :cry,
        description: "Cry",
        file:        "Chatwheel_crybaby.wav"
      }, {
        command:     :disastah,
        description: "It's a disastah",
        file:        "Chatwheel_disastah.wav"
      }, {
        command:     :eto,
        description: "eto prosto netchto",
        file:        "Chatwheel_eto_prosto_netchto.wav"
      }, {
        command:     :patience,
        description: "Patience from Zhou",
        file:        "Chatwheel_patience.wav"
      }, {
        command:     :retard,
        description: "Never go full retard",
        file:        "fullretard.mp3"
      }, {
        command:     :wat,
        description: "Wat?",
        file:        "wat.mp3",
        image:       "http://i.imgur.com/AxtzziK.gif"
      }]

      command(:sounds,
        description:          "List available sound clips.",
        usage:                "#{ZxAlfie.prefix}sounds",
        help_available:       true,
        min_args:             0,
        max_args:             0,
        required_permissions: [:send_messages],
        permission_level:     ZxAlfie.permission_from_identifier(
          :trusted_user
        ).level,
        permission_message:   false
      ) do |event|
        event.channel.send_message(
          CLIPS.reduce("**List of sound clips:**\n") { |memo, c|
            memo + "`#{c[:command]}`, "
          }[0..-3]
        )
        nil
      end

      CLIPS.each do |sound_attributes|
        command(sound_attributes[:command],
          description:          sound_attributes[:description],
          usage:                "#{ZxAlfie.prefix}#{sound_attributes[:command]}",
          help_available:       false,
          min_args:             0,
          max_args:             0,
          required_permissions: [:speak],
          permission_level:     ZxAlfie.permission_from_identifier(
            :trusted_user
          ).level,
          permission_message:   false
        ) do |event|
          next unless ZxAlfie.bot.voice_enabled?

          if event.voice
            event.voice.stop_playing
            event.voice.destroy
          end

          if channel = event.user.voice_channel
            if sound_attributes[:image] && ZxAlfie.bot.discord.profile.on(
                event.server).permission?(:send_messages, event.channel)
              event.channel.send_message sound_attributes[:image]
            end

            voice_bot = ZxAlfie.bot.discord.voice(channel) ||
              ZxAlfie.bot.discord.voice_connect(channel)#, false)
            voice_bot.volume = ZxAlfie.bot.voice_volume
            voice_bot.play_file(File.join(ZxAlfie.root, "mp3",
                                          sound_attributes[:file]))

            voice_bot.destroy
          end

          nil
        end
      end
    end
  end
end
