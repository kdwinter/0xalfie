module ZxAlfie
  module Containers
    module Stats
      extend Discordrb::Commands::CommandContainer

      command(:leaders,
        description:          "Show top 10 users by message count.",
        usage:                "#{ZxAlfie.prefix}leaders",
        help_available:       true,
        min_args:             0,
        max_args:             0,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event|
        leaders = ZxAlfie.bot.db.execute(
          "SELECT user_id, count(user_id) FROM messages WHERE server_id = ? " \
          "GROUP BY user_id ORDER BY count(user_id) DESC LIMIT 10",
          event.server.id
        )

        message = "**Top 10 by message count:**\n\n"

        leaders.each.with_index(1) do |row, index|
          if user = ZxAlfie.bot.discord.user(row["user_id"])
            message << "`#{index}.` **#{user.name}** " \
              "with **#{row["count(user_id)"]}**\n"
          end
        end

        event.channel.send_message(message)
        nil
      end

      command(:quote,
        description:          "Show a random message from given username.",
        usage:                "#{ZxAlfie.prefix}quote alfie",
        help_available:       true,
        min_args:             1,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event, *name|
        name = name.join(" ")
        name = event.user.name if name.empty?
        if member = event.server.members.detect { |m|
          m.name.downcase == name.downcase
        }
          if member.current_bot?
            event.channel.send_message "I won't quote myself."
          else
            quotes = ZxAlfie.bot.db.execute(
              "SELECT content, sent_at FROM messages WHERE user_id = ? AND " \
              "server_id = ? AND channel_id = ?",
              member.id.to_s, event.server.id, event.channel.id
            )

            if quotes.any?
              quote = quotes.sample
              event.channel.send_message(
                "[#{quote["sent_at"].strftime("%d-%m-%Y %H:%M")}] **" \
                "#{member.name}**: #{quote["content"]}"
              )
            else
              event.channel.send_message "I don't have any records for " \
                "**#{member.name}** in this server/channel (yet)."
            end
          end
        else
          event.channel.send_message "There's no user called **#{name}** " \
            "in this server."
        end

        nil
      end

      command(:abusers,
        description:          "Show top 10 users by bot abuse.",
        usage:                "#{ZxAlfie.prefix}abusers",
        help_available:       true,
        min_args:             0,
        max_args:             0,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event|
        abusers = ZxAlfie.bot.db.execute(
          "SELECT user_id, count(user_id) FROM bot_calls WHERE server_id = ? " \
          "GROUP BY user_id ORDER BY count(user_id) DESC LIMIT 10",
          event.server.id
        )
        message = "**Top 10 abusers by command calls** " \
          "(someone please think of Alfie)**:**\n\n"

        abusers.each.with_index(1) do |row, index|
          if user = ZxAlfie.bot.discord.user(row["user_id"])
            message << "`#{index}.` **#{user.name}** with " \
              "**#{row["count(user_id)"]}**\n"
          end
        end

        event.channel.send_message(message)
        nil
      end

      command(:stats,
        description:          "Show top 10 commands by usage count.",
        usage:                "#{ZxAlfie.prefix}stats",
        help_available:       true,
        min_args:             0,
        max_args:             0,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event|
        commands = ZxAlfie.bot.db.execute(
          "SELECT command, count(command) FROM bot_calls WHERE server_id = ? " \
          "GROUP BY command ORDER BY count(command) DESC LIMIT 10",
          event.server.id
        )
        message = "**Top 10 commands by usage count:**\n\n"

        commands.each.with_index(1) do |row, index|
          message << "`#{index}.` `#{row["command"]}` " \
            "with **#{row["count(command)"]}**\n"
        end

        event.channel.send_message(message)
        nil
      end

      command(:messagecount,
        description:          "Show message count for a user.",
        usage:                "#{ZxAlfie.prefix}messagecount alfie",
        help_available:       true,
        min_args:             0,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event, *name|
        name = name.join(" ")
        name = event.user.name if name.empty?

        if member = event.server.members.detect { |m|
          m.name.downcase == name.downcase
        }
          if member.current_bot?
            event.channel.send_message "I'm not counting my own messages " \
              "directly, but you can check `!stats` for detailed command " \
              "usage."
          else
            count = ZxAlfie.bot.db.get_first_value(
              "SELECT count(*) FROM messages WHERE user_id = ? AND server_id = ?",
              member.id.to_s, event.server.id
            )

            evet.channel.send_message "**#{member.name}** sent **#{count}**" \
              "messages so far."
          end
        else
          event.channel.send_message "There's no user called **#{name}** " \
            "in this server."
        end

        nil
      end

      command(:abusecount,
        description:          "Show command call count for a user.",
        usage:                "#{ZxAlfie.prefix}abusecount alfie",
        help_available:       true,
        min_args:             0,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event, *name|
        name = name.join(" ")
        name = event.user.name if name.empty?
        if member = event.server.members.detect { |m|
          m.name.downcase == name.downcase
        }
          if member.current_bot?
            event.channel.send_message "I don't call commands, silly."
          else
            commands = ZxAlfie.bot.db.execute(
              "SELECT command, count(command) FROM bot_calls WHERE user_id = ?" \
              "AND server_id = ? GROUP BY command ORDER BY count(command) DESC " \
              "LIMIT 10",
              member.id.to_s, event.server.id
            )
            message = "**Top 10 most abused commands " \
              "by #{member.name}:**\n\n"
            commands.each.with_index(1) do |row, index|
              message << "`#{index}.` `#{row["command"]}` " \
                "with **#{row["count(command)"]}**\n"
            end

            event.channel.send_message(message)
          end
        else
          event.channel.send_message "There's no user called **#{name}** " \
            "in this server."
        end

        nil
      end
    end
  end
end
