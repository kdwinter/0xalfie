module ZxAlfie
  module Containers
    module Users
      extend Discordrb::Commands::CommandContainer

      @@kicked_users = []
      def self.kicked_users
        @@kicked_users
      end

      @@silenced_users = {}
      def self.silenced_users
        @@silenced_users
      end

      DUMP_FILE = File.join(ZxAlfie.root, "silenced_users.dump")

      def self.bootstrap
        if File.exist?(DUMP_FILE)
          @@silenced_users = (Marshal.load(File.read(DUMP_FILE)) rescue {})
        else
          @@silenced_users = {}
        end
      end

      def self.cleanup
        f = File.open(DUMP_FILE, "w+")
        f.write(Marshal.dump(@@silenced_users))
        f.close
      end

      PERIODIC_INTERVAL = 10
      def self.periodic_tick
        silenced_users.each do |user_id, timeout_ends_at|
          if Time.now.to_i >= timeout_ends_at
            unsilence_user(user_id)
          end
        end
      end

      command(:timeout,
        description:          "Silence a user for a specific duration, " \
                              "in seconds.",
        usage:                "#{ZxAlfie.prefix}timeout 60 alfie",
        help_available:       true,
        min_args:             2,
        permission_level:     ZxAlfie.permission_from_identifier(
          :moderator
        ).level,
        permission_message:   "Nope."
      ) do |event, timeout, *name|
        name = name.join(" ")
        if member = event.server.members.detect { |m|
          m.name.downcase == name.downcase
        }
          unless member.owner? || member.current_bot? || member.id ==
            event.user.id || (event.user.id != ZxAlfie.owner_id &&
              ZxAlfie.bot.discord.permission?(
                member, ZxAlfie.permission_from_identifier(:moderator).level,
                event.server
              )
            )

            ZxAlfie.bot.discord.ignore_user(member)
            silenced_users[member.id] = Time.now.to_i + timeout.to_i
            "**#{member.name}** was silenced for #{timeout} seconds."
          else
            "This user can't be silenced."
          end
        else
          "Couldn't find **#{name}** in the server. Check spelling?"
        end
      end

      command(:untimeout,
        description:          "Unsilence a silenced user.",
        usage:                "#{ZxAlfie.prefix}timeout 60 alfie",
        help_available:       true,
        min_args:             1,
        permission_level:     ZxAlfie.permission_from_identifier(
          :moderator
        ).level,
        permission_message:   "Nope."
      ) do |event, *name|
        name = name.join(" ")
        if member = event.server.members.detect { |m|
          m.name.downcase == name.downcase
        }
          if silenced_users.key?(member.id)
            unsilence_user(member.id)
            "**#{member.name}** was unsilenced."
          else
            "This user isn't silenced."
          end
        else
          "Couldn't find **#{name}** in the server. Check spelling?"
        end
      end

      command(:blame,
        description:          "Someone must be blamed.",
        usage:                "#{ZxAlfie.prefix}blame",
        help_available:       true,
        max_args:             0,
        min_args:             0,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event|
        begin
          user = event.channel.users.sample
        end until not user.current_bot?
        "Someone must be blamed, and it just so happens to be " \
          ":nose::skin-tone-3: **#{user.name}** :nose::skin-tone-3: this time " \
          "around."
      end

      command(:kick,
        description:          "Kick a user.",
        usage:                "#{ZxAlfie.prefix}kick alfie",
        help_available:       true,
        min_args:             1,
        permission_level:     ZxAlfie.permission_from_identifier(
          :moderator
        ).level,
        required_permissions: [:kick_members],
        permission_message:   "Nope."
      ) do |event, *name|
        name = name.join(" ")
        if member = event.server.members.detect { |m|
          m.name.downcase == name.downcase
        }
          unless member.owner? || member.current_bot? || member.id ==
            event.user.id || (event.user.id != ZxAlfie.owner_id &&
              ZxAlfie.bot.discord.permission?(
                member, ZxAlfie.permission_from_identifier(:moderator).level,
                event.server
              )
            )

            event.server.kick(member.id)
            kicked_users.push(member)
            "**#{member.name}** was kicked."
          else
            "I don't kick cool people :sunglasses:."
          end
        else
          "Couldn't find **#{name}** in the server. Check spelling!"
        end
      end

      # TODO: Take a new look at this once we can generate new invite links.
=begin
      command(:reinvite,
        description:        "Reinvite a kicked user.",
        usage:              "#{ZxAlfie.prefix}reinvite alfie",
        help_available:     true,
        min_args:           1,
        permission_level:   ZxAlfie.permission_from_identifier(:moderator).level,
        permission_message: "You can't."
      ) do |event, *name|
        name = name.join(" ")
        if member = kicked_users.detect { |m| m.name.downcase == name.downcase }
          member.pm "Someone kicked you and #{event.user.name} thought you should be reinvited, so here you go: #{PERM_INVITE_LINK}"
          if ZxAlfie.bot.discord.profile.on(event.server).permission?(:send_messages, event.channel)
            "Sent an invite link to **#{member.name}**."
          end
        else
          if ZxAlfie.bot.discord.profile.on(event.server).permission?(:send_messages, event.channel)
            "This person hasn't been recently kicked, or at least I have no recent memory of it..."
          end
        end
      end
=end

      command(:seen,
        description:          "Show the time a user was last seen.",
        usage:                "#{ZxAlfie.prefix}seen alfie",
        help_available:       true,
        min_args:             1,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event, *name|
        name = name.join(" ")
        if member = event.server.members.detect { |m|
          m.name.downcase == name.downcase
        }
          if row = ZxAlfie.bot.db.get_first_row(
            "SELECT seen_at FROM seen WHERE user_id = ? AND server_id = ?",
            member.id, event.server.id
          )
            "I last saw **#{member.name}** at **" \
              "#{row["seen_at"].strftime("%d/%m/%y %H:%M")}**."
          else
            "I have no information on **#{member.name}** so far."
          end
        else
          "There's no user called **#{name}** in this server."
        end
      end

      def self.unsilence_user(user_id)
        ZxAlfie.bot.logger.info("Unsilencing #{user_id}")

        ZxAlfie.bot.discord.unignore_user(user_id)
        silenced_users.delete(user_id)
      end
    end
  end
end
