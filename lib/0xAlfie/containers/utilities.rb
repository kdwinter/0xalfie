module ZxAlfie
  module Containers
    module Utilities
      extend Discordrb::Commands::CommandContainer

      command(:eval,
        description:          "Evaluate.",
        usage:                "#{ZxAlfie.prefix}eval ZxAlfie.bot.voice_enabled?",
        help_available:       false,
        min_args:             1,
        permission_level:     ZxAlfie.permission_from_identifier(
          :administrator
        ).level,
        permission_message:   false
      ) do |event, *args|
        break if event.user.id != ZxAlfie.owner_id

        begin
          event.channel.send_message(eval args.join(" "))
        rescue => e
          event.channel.send_message "Exception caught: #{e.message}"
        end

        nil
      end

      command(:calc,
        description:          "Basic arithmetic.",
        usage:                "#{ZxAlfie.prefix}calc 1+2",
        help_available:       true,
        min_args:             1,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event, *args|
        args = args.join(" ")
        # dentaku is some random library that takes a string and evaluates
        # the math inside, so I don't have to write it myself
        begin
          result = Dentaku::Calculator.new.evaluate(args) || "I'm confused"
        rescue StandardError
          result = "fuck that shit"
        end

        event.channel.send_message "According to my calculations, " \
          "**#{args}** = **#{result.to_f}**"
        nil
      end

      command(:search,
        description:          "Search google and return the first result.",
        usage:                "#{ZxAlfie.prefix}search some random shit",
        help_available:       true,
        min_args:             1,
        required_permissions: [:send_messages, :embed_links],
        permission_message:   false
      ) do |event, *search|
        search = search.join(" ")
        begin
          result = ZxAlfie::API.google.execute!(
            api_method: ZxAlfie::API.customsearch.cse.list, parameters: {
              q: search, cx: ZxAlfie.config[:api][:google_search_key], num: 1,
              safe: "off"
            }
          )
          if result = JSON.parse(result.response.body)["items"]
            result = result.first
            event.channel.send_message "#{result["title"]}: #{result["link"]}"
          else
            event.channel.send_message "No results found."
          end

          nil
        rescue StandardError => e
          event.channel.send_message "Something went wrong searching your " \
            ":shit:. Error was logged."
          ZxAlfie.bot.discord.user(ZxAlfie.owner_id).pm "Error while googling:" \
            "`#{e.message[0..1900]}`"
          nil
        end
      end

      command(:image,
        description:          "Search google images and return the first image.",
        usage:                "#{ZxAlfie.prefix}image ass",
        help_available:       true,
        min_args:             1,
        required_permissions: [:send_messages, :embed_links],
        permission_message:   false
      ) do |event, *search|
        search = search.join(" ")
        begin
          result = ZxAlfie::API.google.execute!(
            api_method: ZxAlfie::API.customsearch.cse.list, parameters: {
              q: search, cx: ZxAlfie.config[:api][:google_search_key], num: 1,
              safe: "off", searchType: "image"
            }
          )
          if result = JSON.parse(result.response.body)["items"]
            result = result.first
            event.channel.send_message "#{result["title"]}: #{result["link"]}"
          else
            event.channel.send_message "No results found."
          end

          nil
        rescue StandardError => e
          event.channel.send_message "Something went wrong searching your " \
            ":shit:. Error was logged."
          ZxAlfie.bot.discord.user(ZxAlfie.owner_id).pm "Error while google" \
            "imaging: `#{e.message[0..1900]}`"
          nil
        end
      end

      command(:youtube,
        description:          "Search for a youtube video.",
        usage:                "#{ZxAlfie.prefix}youtube some random vid",
        help_available:       true,
        min_args:             1,
        required_permissions: [:send_messages, :embed_links],
        permission_message:   false
      ) do |event, *search|
        search = search.join(" ")
        begin
          result = ZxAlfie::API.google.execute!(
            api_method: ZxAlfie::API.youtube.search.list, parameters: {
              part: "snippet", q: search, maxResults: 1, type: "video",
              safeSearch: "none"
            }
          )

          if result = result.data.items.first
            event.channel.send_message "https://youtube.com/watch?v=#{result.id.videoId}"
          else
            event.channel.send_message "No results found."
          end

          nil
        rescue StandardError => e
          event.channel.send_message "Something went wrong trying to find " \
            "your goddamn video. Error was logged."
          ZxAlfie.bot.discord.user(ZxAlfie.owner_id).pm "Error while youtubing:" \
            "`#{e.message[0..1900]}`"
          nil
        end
      end

      command(:urban,
        description:          "Search Urban Dictionary for a definition. Only " \
                              "the first match is returned.",
        usage:                "#{ZxAlfie.prefix}urban meme",
        help_available:       true,
        min_args:             1,
        required_permissions: [:send_messages, :embed_links],
        permission_message:   false
      ) do |event, *search|
        search = search.join(" ")
        begin
          result = Net::HTTP.get_response(URI.parse(
            "http://api.urbandictionary.com/v0/define?term=#{search}"
          ))
          result = JSON.parse(result.body)["list"].first

          message = "**#{search}** on Urban Dictionary:  "
          message << result["definition"]

          event.channel.send_message(message)
        rescue StandardError => e
          event.channel.send_message "Something went wrong. Error was logged."
          ZxAlfie.bot.discord.user(ZxAlfie.owner_id).pm "Error while urban " \
            "dictionarying: `#{e.message[0..1900]}`"
        end
        nil
      end

      EIGHTBALL_ANSWERS = [
        "yes", "no", "outlook not so good", "all signs point to yes",
        "all signs point to no", "why the hell are you asking me?",
        "the answer is unclear"
      ].freeze.map(&:freeze)
      command(:eightball,
        description:          "Ask the magic eightball.",
        usage:                "#{ZxAlfie.prefix}eightball am I stupid",
        help_available:       true,
        min_args:             0,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event, *question|
        event.channel.send_message "*shaking the magic 8-ball...* " \
          "#{EIGHTBALL_ANSWERS.sample}"
        nil
      end

      command(:roll,
        description:          "Roll a random number.",
        usage:                "#{ZxAlfie.prefix}roll, #{ZxAlfie.prefix}roll 100",
        help_available:       true,
        min_args:             0,
        max_args:             1,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event, max|
        roll =
          if max
            rand(1..max.to_i)
          else
            rand(1..100)
          end

        event.channel.send_message "**#{event.user.name}** " \
          "rolled **#{roll}** (1-#{(max || 100).to_i})."
        nil
      end

      command(:coinflip,
        description:          "Flip a coin.",
        usage:                "#{ZxAlfie.prefix}coinflip",
        help_available:       true,
        min_args:             0,
        max_args:             0,
        required_permissions: [:send_messages],
        permission_message:   false
      ) do |event|
        result = %w(Heads Tails).sample
        event.channel_send_message "**#{event.user.name}** flipped " \
          "a coin and got **#{result}**."
        nil
      end

      command(:ddos,
        description:          "Fuck him up.",
        usage:                "#{ZxAlfie.prefix}ddos Bald",
        help_available:       true,
        min_args:             1,
        required_permissions: [:speak, :manage_channels, :manage_server],
        permission_level:     ZxAlfie.permission_from_identifier(
          :moderator
        ).level,
        permission_message:   false
      ) do |event, *name|
        name = name.join(" ")
        member = event.server.members.detect { |m|
          m.name.downcase == name.downcase
        }

        if member && member.id != ZxAlfie.owner_id
          if channel = member.voice_channel
            other_channel = event.server.channels.reject { |c|
              c.id == channel.id
            }.sample

            3.times do
              event.server.move(member, other_channel)
              sleep 0.5
              event.server.move(member, channel)
              sleep 0.5
            end

            nil
          else
            event.channel.send_message "**#{name}** is not in a voice " \
              "channel."
          end
        else
          event.channel_send_message "Couldn't find **#{name}** in " \
            "the server. Check spelling?"
        end

        nil
      end

    end
  end
end
