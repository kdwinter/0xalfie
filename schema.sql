CREATE TABLE IF NOT EXISTS messages (
  content    text         NOT NULL,
  channel_id varchar(255) NOT NULL,
  user_id    varchar(255) NOT NULL,
  server_id  varchar(255) NOT NULL,
  sent_at    datetime     NOT NULL
);

CREATE TABLE IF NOT EXISTS commands (
  key        varchar(255) NOT NULL,
  value      text         NOT NULL,
  user_id    varchar(255) NOT NULL,
  server_id  varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS seen (
  status     text,
  user_id    varchar(255) NOT NULL,
  server_id  varchar(255) NOT NULL,
  seen_at    datetime     NOT NULL
);

CREATE TABLE IF NOT EXISTS bot_calls (
  command    varchar(255) NOT NULl,
  user_id    varchar(255) NOT NULL,
  channel_id varchar(255) NOT NULL,
  server_id  varchar(255) NOT NULL,
  called_at  datetime     NOT NULL
);

CREATE TABLE IF NOT EXISTS user_permissions (
  user_id        varchar(255) NOT NULL,
  server_id      varchar(255) NOT NULL,
  identifier     varchar(255) NOT NULL,
  set_by_user_id varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS role_permissions (
  role_id        varchar(255) NOT NULL,
  server_id      varchar(255) NOT NULL,
  identifier     varchar(255) NOT NULL,
  set_by_user_id varchar(255) NOT NULL
);
